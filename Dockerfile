FROM docker.elastic.co/logstash/logstash:7.11.2

RUN mkdir -p ~/INPUT1

RUN chmod 777 /usr/share/logstash/INPUT1

ADD input.csv /usr/share/logstash/INPUT1/

ADD logstash.yml /usr/share/logstash/config/logstash.yml

ADD logstash.conf /usr/share/logstash/pipeline/logstash.conf

